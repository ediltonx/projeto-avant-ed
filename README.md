# projeto-avant-ed

projeto para aprender devops

## Melzinho na chupeta

[  ] Criar a pasta para a aplicação
[  ] Criar uma aplicação Web Básica

A aplicação pode ser uma página html básica (index.html). Se desejar utilize algum template da internet ou crie o seu próprio site.


[  ]  Criar um projeto no GitLab
[  ] Realizar o primeiro Commit

# git init --initial-branch=main
# git remote add origin
# https://gitlab.com/seuusuário/seuprojeto.git
# git add .
# git commit -m "Initial commit"
# git push --set-upstream origin main


Certifique-se o git esteja instalado em seu computador
Em alguns casos, principalmente se estiver utilizando uma máquina Linux será necessário criar um Token de acesso.


[  ] Criar um Dockerfile na pasta da aplicação.

FROM httpd:latest
WORKDIR /usr/local/apache2/htdocs/
COPY * /usr/local/apache2/htdocs/
EXPOSE 80

[OPS]

[  ] Criar uma VM Linux Ubuntu que será utilizada como servidor da aplicação.
[  ] Ter acesso remoto ao servidor criado via putty ou ssh (ou utilizando o vagrant ssh em caso de um servidor local).
[  ] Atualizar o servidor

# sudo apt-get update
# sudo apt-get upgrade -y

[  ] Instalar o Docker

# curl -fsSL https://get.docker.com -o get-docker.sh
# sudo sh get-docker.sh

[   ] Instalar o GitLab-Runner

# curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
# sudo apt-get install gitlab-runner

[  ] Registar e iniciar o GitLab-Runner

# sudo gitlab-runner register
# sudo gitlab-runner start

[  ] Dar permissões para uso do Docker para o usuário gitlab-runner

# sudo usermod -G docker gitlab-runner
# sudo passwd gitlab-runner
# su gitlab-runner

[ ] Logar na conta do Docker Hub

# docker login



[DevOps]

[  ] Criar o arquivo .gitlab-ci.yml para dar início ao pipeline de deploy da aplicação.

stages:
  - build
  - deploy

criar_imagens:
  stage: build
  tags:
    - vagrant
  script:
  - docker build -t seuusuario/projetao:1.0 app/.
  - docker push seuusuario/projetao:1.0



[  ] Finalizar o pipeline

publicar_imagens:
  stage: deploy
  tags:
    - vagrant
  needs:
    - criar_imagens
  script:
  - docker run -dti --name web-server -p 80:80 edilton/projeto:1.0
